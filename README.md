# Deploying Traefik on Docker Swarm

To deploy;

docker stack deploy -c traefik_noprism.yml traefik

Full tutorial;

https://fluffycloudsandlines.blog/using-traefik-on-docker-swarm/