variable "public_subnets" {
  type = "list"
}

variable "public_subnet_cidrs" {
  type = "list"
}

variable "environment_name" {
  
}

variable "vpc_id" {
  
}

variable "dns_root" {
  
}

