data "aws_ami" "base_image" {
  owners      = ["self"]
  most_recent = true

  filter {
    name   = "name"
    values = ["dockerswarm*"]
  }
}

resource "aws_launch_template" "ec2lt" {
  name = "dockerswarm"

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_size = 10
    }
  }

  disable_api_termination = false
  image_id                = "${data.aws_ami.base_image.id}"
  instance_type           = "t2.micro"
  key_name                = "dockerswarm"
  vpc_security_group_ids  = ["${aws_security_group.sg-SSH.id}", "${aws_security_group.sg-Docker.id}"]

  #user_data = "${base64encode(data.template_file.userdata.rendered)}"

  tags = {
    Terraform   = "true"
    Environment = "${var.environment_name}"
    role        = "node"
  }

  tag_specifications = {
    resource_type = "instance"

    tags = {
      Terraform   = "true"
      Environment = "${var.environment_name}"
      role        = "node"
    }
  }
}

#data "template_file" "userdata" {
#  template = "${file("${path.module}/userdata.tmpl")}"
#  enabled = false
#  vars = {
#  }
#}

resource "aws_autoscaling_group" "asg" {
  health_check_type         = "EC2"
  health_check_grace_period = 300
  vpc_zone_identifier       = ["${var.public_subnets}"]
  desired_capacity          = 3
  max_size                  = 3
  min_size                  = 3

  launch_template = {
    id      = "${aws_launch_template.ec2lt.id}"
    version = "$$Latest"
  }

  tags = [
    {
      key                 = "Terraform"
      value               = "true"
      propagate_at_launch = true
    },
    {
      key                 = "Environment"
      value               = "${var.environment_name}"
      propagate_at_launch = true
    },
  ]
}

resource "aws_autoscaling_policy" "asgpolicy" {
  name                   = "Docker Swarm ASG Policy"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 360
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"
}

resource "aws_security_group" "sg-SSH" {
  name   = "Docker Swarm - Edge"
  vpc_id = "${var.vpc_id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    # TLS (change to whatever ports you need)
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    # TLS (change to whatever ports you need)
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Terraform   = "true"
    Environment = "${var.environment_name}"
  }
}

resource "aws_security_group" "sg-Docker" {
  name   = "Docker Swarm - RPC"
  vpc_id = "${var.vpc_id}"

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 2377
    to_port   = 2377
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["${var.public_subnet_cidrs}"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 2377
    to_port   = 2377
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["${var.public_subnet_cidrs}"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 7946
    to_port   = 7946
    protocol  = "tcp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["${var.public_subnet_cidrs}"]
  }
  ingress {
    # TLS (change to whatever ports you need)
    from_port = 7946
    to_port   = 7946
    protocol  = "udp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["${var.public_subnet_cidrs}"]
  }

  ingress {
    # TLS (change to whatever ports you need)
    from_port = 4789
    to_port   = 4789
    protocol  = "udp"

    # Please restrict your ingress to only necessary IPs and ports.
    # Opening to 0.0.0.0/0 can lead to security vulnerabilities.
    cidr_blocks = ["${var.public_subnet_cidrs}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Terraform   = "true"
    Environment = "${var.environment_name}"
  }
}
