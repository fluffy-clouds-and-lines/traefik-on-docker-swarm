# Docker Swarm deployment using Terraform

Build our image:

packer build ./packer/template.json

Deploy the Swarm nodes:

terraform init
terraform deploy