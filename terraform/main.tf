variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "environment_name" {}

variable "azs" {
  type = "list"
}

variable "database_subnets" {
  type = "list"
}

variable "public_subnets" {
  type = "list"
}

variable "dns_root" {}

provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region     = "eu-west-2"
}

module "network" {
  source           = "./modules/network/"
  azs              = "${var.azs}"
  database_subnets = "${var.database_subnets}"
  public_subnets   = "${var.public_subnets}"
  environment_name = "${var.environment_name}"
}

module "compute" {
  source           = "./modules/compute/"
  vpc_id           = "${module.network.vpc_id}"
  public_subnets   = "${module.network.public_subnets}"
  public_subnet_cidrs = "${var.public_subnets}"
  environment_name = "${var.environment_name}"
  dns_root         = "${var.dns_root}"
}
