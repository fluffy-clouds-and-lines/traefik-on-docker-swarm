# Tag should change depending on your environment
environment_name = "dockerswarm-lab" 
# Change to your local Availability Zones
azs = ["eu-west-2a", "eu-west-2b", "eu-west-2c"] 
database_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"] 
public_subnets = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"] 
# For example nextamazing.site
dns_root = "nextamazing.site"
# An IAM user with Power User or Administrator privileges
aws_access_key = ""
aws_secret_key = ""
